#!/usr/bin/env ruby

require "optparse"

options = {}

optparse = OptionParser.new do |opts|
  opts.banner = "Usage: new.rb [options] project_name"
end

if ARGV.empty?
  puts "Please provide a project name"
  return
end

ARGV.each do |a|
  file = File.expand_path("../#{a}")
  this_script = File.expand_path(File.dirname(File.dirname(__FILE__)))

  if File.exist?(file)
    puts "A project by that name already exists."
    return
  end

  exec "cp -r #{this_script} #{file}"
  puts "Created #{file}"
end