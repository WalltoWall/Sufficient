Sufficient Frontend Framework
=============================
An opinionated frontend development framework for people behind deadlines.

Libraries Used
--------------
- Compass (http://compass-style.org)
- HTML5Boilerplate (http://html5boilerplate.com/)
- Less Framework 4 (http://lessframework.com/)
- Frameless Grid (http://framelessgrid.com/)

Assumptions
-----------
1. We assume you're building a responsive website.
2. We assume you're using Compass.
3. We assume you don't want to keep compiled CSS files versioned, since SCSS is a better source code.
4. We assume you like Paul Irish.
5. We assume you believe box-sizing: border-box; is an improvement on the box model.
6. We assume you don't want to waste time learning how to create grids in HTML. Why not harness the power of SCSS and a [Frameless Grid](http://framelessgrid.com/)?

Authors
-------
[Wall-to-Wall Studios](http://walltowall.com)